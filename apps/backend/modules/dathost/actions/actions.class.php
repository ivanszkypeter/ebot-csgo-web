<?php

class dathostActions extends sfActions {

    private function __($text, $args = array()) {
        return $this->getContext()->getI18N()->__($text, $args, 'messages');
    }

    /**
     * Executes index action
     *
     * @param sfRequest $request A request object
     */
    public function executeStart(sfWebRequest $request) {
        try {
            $DatHostAPI = new DatHostAPI();
            $DatHostAPI->postGameServerStart('5eac59795b44d989a0bcb77f');
        } catch (Exception $e) {
            return $this->renderText(json_encode(array(
                "status" => false)));
        }
        return $this->renderText(json_encode(array(
            "status" => true)));
    }

    public function executeStop(sfWebRequest $request) {
        try {
            $DatHostAPI = new DatHostAPI();
            $DatHostAPI->postGameServerStop('5eac59795b44d989a0bcb77f');
        } catch (Exception $e) {
            return $this->renderText(json_encode(array(
                "status" => false)));
        }
        return $this->renderText(json_encode(array(
            "status" => true)));
    }
}
