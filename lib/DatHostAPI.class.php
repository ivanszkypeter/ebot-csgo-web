<?php
require_once(__DIR__.'/../plugins/datHostClient/vendor/autoload.php');

class DatHostAPI extends Swagger\Client\Api\DefaultApi
{

    public function __construct()
    {
        parent::__construct(new GuzzleHttp\Client([
            'auth' => [
                'esportmanager.office@gmail.com',
                'K3csk3Sajt'
            ]
        ]));
    }

}