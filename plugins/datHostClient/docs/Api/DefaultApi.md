# Swagger\Client\DefaultApi

All URIs are relative to *https://dathost.net*

Method | HTTP request | Description
------------- | ------------- | -------------
[**deleteGameServerFilesItem**](DefaultApi.md#deleteGameServerFilesItem) | **DELETE** /api/0.1/game-servers/{server_id}/files/{path} | Delete a file/directory from the gameserver
[**deleteGameServerItem**](DefaultApi.md#deleteGameServerItem) | **DELETE** /api/0.1/game-servers/{server_id} | Delete a server
[**getAccount**](DefaultApi.md#getAccount) | **GET** /api/0.1/account | Get info about the account
[**getCustomDomains**](DefaultApi.md#getCustomDomains) | **GET** /api/0.1/custom-domains | 
[**getGameServerBackups**](DefaultApi.md#getGameServerBackups) | **GET** /api/0.1/game-servers/{server_id}/backups | List all available backups for this server
[**getGameServerConsole**](DefaultApi.md#getGameServerConsole) | **GET** /api/0.1/game-servers/{server_id}/console | Get the last lines of backlog from the server console
[**getGameServerFiles**](DefaultApi.md#getGameServerFiles) | **GET** /api/0.1/game-servers/{server_id}/files | List files on gameserver
[**getGameServerFilesItem**](DefaultApi.md#getGameServerFilesItem) | **GET** /api/0.1/game-servers/{server_id}/files/{path} | Download a file from the gameserver
[**getGameServerItem**](DefaultApi.md#getGameServerItem) | **GET** /api/0.1/game-servers/{server_id} | Get all info about one gameserver
[**getGameServerMetrics**](DefaultApi.md#getGameServerMetrics) | **GET** /api/0.1/game-servers/{server_id}/metrics | 
[**getGameServers**](DefaultApi.md#getGameServers) | **GET** /api/0.1/game-servers | Get a list of all non-deleted gameservers
[**postGameServerBackupsRestore**](DefaultApi.md#postGameServerBackupsRestore) | **POST** /api/0.1/game-servers/{server_id}/backups/{backup_name}/restore | Restore all files from a backup
[**postGameServerConsole**](DefaultApi.md#postGameServerConsole) | **POST** /api/0.1/game-servers/{server_id}/console | Send a line of text to the server console
[**postGameServerDuplicate**](DefaultApi.md#postGameServerDuplicate) | **POST** /api/0.1/game-servers/{server_id}/duplicate | Duplicate a server (copies settings and files to a new server)
[**postGameServerFilesItem**](DefaultApi.md#postGameServerFilesItem) | **POST** /api/0.1/game-servers/{server_id}/files/{path} | Upload a file to the gameserver
[**postGameServerRegenerateFtpPassword**](DefaultApi.md#postGameServerRegenerateFtpPassword) | **POST** /api/0.1/game-servers/{server_id}/regenerate-ftp-password | Generate a new FTP password
[**postGameServerReset**](DefaultApi.md#postGameServerReset) | **POST** /api/0.1/game-servers/{server_id}/reset | Reset all file changes and settings
[**postGameServerStart**](DefaultApi.md#postGameServerStart) | **POST** /api/0.1/game-servers/{server_id}/start | Start a server
[**postGameServerStop**](DefaultApi.md#postGameServerStop) | **POST** /api/0.1/game-servers/{server_id}/stop | Stop a server
[**postGameServerSyncFiles**](DefaultApi.md#postGameServerSyncFiles) | **POST** /api/0.1/game-servers/{server_id}/sync-files | Sync the files between the API cache and the gameserver
[**postGameServerUnzip**](DefaultApi.md#postGameServerUnzip) | **POST** /api/0.1/game-servers/{server_id}/unzip/{path} | Unzip a file on the GameServer
[**postGameServers**](DefaultApi.md#postGameServers) | **POST** /api/0.1/game-servers | Create server
[**putGameServerFilesItem**](DefaultApi.md#putGameServerFilesItem) | **PUT** /api/0.1/game-servers/{server_id}/files/{path} | Move a file/directory on the gameserver
[**putGameServerItem**](DefaultApi.md#putGameServerItem) | **PUT** /api/0.1/game-servers/{server_id} | Update server details


# **deleteGameServerFilesItem**
> deleteGameServerFilesItem($server_id, $path)

Delete a file/directory from the gameserver

The path is counted from the root node as seen in the file manager in the control panel, i.e. to delete csgo/cfg/server.cfg the path would be cfg/server.cfg.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Swagger\Client\Api\DefaultApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$server_id = "server_id_example"; // string | 
$path = "path_example"; // string | 

try {
    $apiInstance->deleteGameServerFilesItem($server_id, $path);
} catch (Exception $e) {
    echo 'Exception when calling DefaultApi->deleteGameServerFilesItem: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **server_id** | **string**|  |
 **path** | **string**|  |

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **deleteGameServerItem**
> deleteGameServerItem($server_id)

Delete a server

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Swagger\Client\Api\DefaultApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$server_id = "server_id_example"; // string | 

try {
    $apiInstance->deleteGameServerItem($server_id);
} catch (Exception $e) {
    echo 'Exception when calling DefaultApi->deleteGameServerItem: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **server_id** | **string**|  |

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **getAccount**
> \Swagger\Client\Model\Account getAccount($x_fields)

Get info about the account

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Swagger\Client\Api\DefaultApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$x_fields = "x_fields_example"; // string | An optional fields mask

try {
    $result = $apiInstance->getAccount($x_fields);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling DefaultApi->getAccount: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **x_fields** | **string**| An optional fields mask | [optional]

### Return type

[**\Swagger\Client\Model\Account**](../Model/Account.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **getCustomDomains**
> \Swagger\Client\Model\CustomDomain[] getCustomDomains($x_fields)



### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Swagger\Client\Api\DefaultApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$x_fields = "x_fields_example"; // string | An optional fields mask

try {
    $result = $apiInstance->getCustomDomains($x_fields);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling DefaultApi->getCustomDomains: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **x_fields** | **string**| An optional fields mask | [optional]

### Return type

[**\Swagger\Client\Model\CustomDomain[]**](../Model/CustomDomain.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **getGameServerBackups**
> getGameServerBackups($server_id, $x_fields)

List all available backups for this server

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Swagger\Client\Api\DefaultApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$server_id = "server_id_example"; // string | 
$x_fields = "x_fields_example"; // string | An optional fields mask

try {
    $apiInstance->getGameServerBackups($server_id, $x_fields);
} catch (Exception $e) {
    echo 'Exception when calling DefaultApi->getGameServerBackups: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **server_id** | **string**|  |
 **x_fields** | **string**| An optional fields mask | [optional]

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **getGameServerConsole**
> getGameServerConsole($server_id, $x_fields, $max_lines)

Get the last lines of backlog from the server console

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Swagger\Client\Api\DefaultApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$server_id = "server_id_example"; // string | 
$x_fields = "x_fields_example"; // string | An optional fields mask
$max_lines = 1000; // int | Maximum number of lines to fetch (1-1000)

try {
    $apiInstance->getGameServerConsole($server_id, $x_fields, $max_lines);
} catch (Exception $e) {
    echo 'Exception when calling DefaultApi->getGameServerConsole: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **server_id** | **string**|  |
 **x_fields** | **string**| An optional fields mask | [optional]
 **max_lines** | **int**| Maximum number of lines to fetch (1-1000) | [optional] [default to 1000]

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **getGameServerFiles**
> getGameServerFiles($server_id, $hide_default_files, $path, $with_filesizes)

List files on gameserver

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Swagger\Client\Api\DefaultApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$server_id = "server_id_example"; // string | 
$hide_default_files = "hide_default_files_example"; // string | If true, only files added by the user will be shown, default is all files
$path = "path_example"; // string | Path to use as root, leave empty to get all files
$with_filesizes = "with_filesizes_example"; // string | If true, return filesizes with filenames

try {
    $apiInstance->getGameServerFiles($server_id, $hide_default_files, $path, $with_filesizes);
} catch (Exception $e) {
    echo 'Exception when calling DefaultApi->getGameServerFiles: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **server_id** | **string**|  |
 **hide_default_files** | **string**| If true, only files added by the user will be shown, default is all files | [optional]
 **path** | **string**| Path to use as root, leave empty to get all files | [optional]
 **with_filesizes** | **string**| If true, return filesizes with filenames | [optional]

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **getGameServerFilesItem**
> getGameServerFilesItem($server_id, $path, $as_text)

Download a file from the gameserver

The path is counted from the root node as seen in the file manager in the control panel, i.e. to retrieve csgo/cfg/server.cfg the path would be cfg/server.cfg  If the path is a directory you will receive a zip file with the directory's contents  Note that files are retrieved from an API cache which is synced periodically (approximately once per minute) from the gameserver, to make sure all files on the server is retrieveble, run /game-servers/{server_id}/sync-files first.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Swagger\Client\Api\DefaultApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$server_id = "server_id_example"; // string | 
$path = "path_example"; // string | 
$as_text = "as_text_example"; // string | If true, files up to 100kB will be returned as text/plain and bigger files will return an error

try {
    $apiInstance->getGameServerFilesItem($server_id, $path, $as_text);
} catch (Exception $e) {
    echo 'Exception when calling DefaultApi->getGameServerFilesItem: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **server_id** | **string**|  |
 **path** | **string**|  |
 **as_text** | **string**| If true, files up to 100kB will be returned as text/plain and bigger files will return an error | [optional]

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **getGameServerItem**
> \Swagger\Client\Model\GameServer getGameServerItem($server_id, $x_fields)

Get all info about one gameserver

If the server is currently in the booting state it will also check if the server           is still booting and update the value before returning

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Swagger\Client\Api\DefaultApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$server_id = "server_id_example"; // string | 
$x_fields = "x_fields_example"; // string | An optional fields mask

try {
    $result = $apiInstance->getGameServerItem($server_id, $x_fields);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling DefaultApi->getGameServerItem: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **server_id** | **string**|  |
 **x_fields** | **string**| An optional fields mask | [optional]

### Return type

[**\Swagger\Client\Model\GameServer**](../Model/GameServer.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **getGameServerMetrics**
> \Swagger\Client\Model\Metrics getGameServerMetrics($server_id, $x_fields)



### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Swagger\Client\Api\DefaultApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$server_id = "server_id_example"; // string | 
$x_fields = "x_fields_example"; // string | An optional fields mask

try {
    $result = $apiInstance->getGameServerMetrics($server_id, $x_fields);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling DefaultApi->getGameServerMetrics: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **server_id** | **string**|  |
 **x_fields** | **string**| An optional fields mask | [optional]

### Return type

[**\Swagger\Client\Model\Metrics**](../Model/Metrics.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **getGameServers**
> \Swagger\Client\Model\GameServer[] getGameServers($x_fields)

Get a list of all non-deleted gameservers

The booting state is not updated using this method, so a server might show true when            booting is finished. Use single item get to check booting state.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Swagger\Client\Api\DefaultApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$x_fields = "x_fields_example"; // string | An optional fields mask

try {
    $result = $apiInstance->getGameServers($x_fields);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling DefaultApi->getGameServers: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **x_fields** | **string**| An optional fields mask | [optional]

### Return type

[**\Swagger\Client\Model\GameServer[]**](../Model/GameServer.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **postGameServerBackupsRestore**
> postGameServerBackupsRestore($server_id, $backup_name)

Restore all files from a backup

This will reset all changed files to the backups files and void any changes you have made afterwards. It's not possible to revert this change.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Swagger\Client\Api\DefaultApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$server_id = "server_id_example"; // string | 
$backup_name = "backup_name_example"; // string | 

try {
    $apiInstance->postGameServerBackupsRestore($server_id, $backup_name);
} catch (Exception $e) {
    echo 'Exception when calling DefaultApi->postGameServerBackupsRestore: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **server_id** | **string**|  |
 **backup_name** | **string**|  |

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **postGameServerConsole**
> postGameServerConsole($server_id, $line)

Send a line of text to the server console

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Swagger\Client\Api\DefaultApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$server_id = "server_id_example"; // string | 
$line = "line_example"; // string | A line of text to send to the server console

try {
    $apiInstance->postGameServerConsole($server_id, $line);
} catch (Exception $e) {
    echo 'Exception when calling DefaultApi->postGameServerConsole: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **server_id** | **string**|  |
 **line** | **string**| A line of text to send to the server console |

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/x-www-form-urlencoded, multipart/form-data
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **postGameServerDuplicate**
> \Swagger\Client\Model\GameServer postGameServerDuplicate($server_id, $x_fields)

Duplicate a server (copies settings and files to a new server)

Please note that the server's files are duplicated from a local cache, to make sure the latest file changes are preserved, update the local cache with the sync-files method first.  Returns the new server.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Swagger\Client\Api\DefaultApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$server_id = "server_id_example"; // string | 
$x_fields = "x_fields_example"; // string | An optional fields mask

try {
    $result = $apiInstance->postGameServerDuplicate($server_id, $x_fields);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling DefaultApi->postGameServerDuplicate: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **server_id** | **string**|  |
 **x_fields** | **string**| An optional fields mask | [optional]

### Return type

[**\Swagger\Client\Model\GameServer**](../Model/GameServer.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **postGameServerFilesItem**
> postGameServerFilesItem($server_id, $path, $file)

Upload a file to the gameserver

The path is counted from the root node as seen in the file manager in the control panel, i.e. to write csgo/cfg/server.cfg the path would be cfg/server.cfg, if the path ends with / a directory will be created and the file parameter will be ignored.  There is a upload limit of 100MB on dathost.net, use upload.dathost.net (with the same path) to upload files up to 500MB.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Swagger\Client\Api\DefaultApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$server_id = "server_id_example"; // string | 
$path = "path_example"; // string | 
$file = "/path/to/file.txt"; // \SplFileObject | The file to be uploaded as a multipart/form-data body <a target=\"_blank\" href=\"         http://docs.python-requests.org/en/latest/user/quickstart/#post-a-multipart-encoded-file\">         (Python example)</a>, if not provided an empty file/directory will be created

try {
    $apiInstance->postGameServerFilesItem($server_id, $path, $file);
} catch (Exception $e) {
    echo 'Exception when calling DefaultApi->postGameServerFilesItem: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **server_id** | **string**|  |
 **path** | **string**|  |
 **file** | **\SplFileObject**| The file to be uploaded as a multipart/form-data body &lt;a target&#x3D;\&quot;_blank\&quot; href&#x3D;\&quot;         http://docs.python-requests.org/en/latest/user/quickstart/#post-a-multipart-encoded-file\&quot;&gt;         (Python example)&lt;/a&gt;, if not provided an empty file/directory will be created | [optional]

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: multipart/form-data
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **postGameServerRegenerateFtpPassword**
> postGameServerRegenerateFtpPassword($server_id)

Generate a new FTP password

Sets a new random FTP password for the server

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Swagger\Client\Api\DefaultApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$server_id = "server_id_example"; // string | 

try {
    $apiInstance->postGameServerRegenerateFtpPassword($server_id);
} catch (Exception $e) {
    echo 'Exception when calling DefaultApi->postGameServerRegenerateFtpPassword: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **server_id** | **string**|  |

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **postGameServerReset**
> postGameServerReset($server_id)

Reset all file changes and settings

This will reset all changed files to the default files, settings to the default values and void any changes you have made. It's not possible to revert this change.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Swagger\Client\Api\DefaultApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$server_id = "server_id_example"; // string | 

try {
    $apiInstance->postGameServerReset($server_id);
} catch (Exception $e) {
    echo 'Exception when calling DefaultApi->postGameServerReset: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **server_id** | **string**|  |

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **postGameServerStart**
> postGameServerStart($server_id, $allow_host_reassignment)

Start a server

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Swagger\Client\Api\DefaultApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$server_id = "server_id_example"; // string | 
$allow_host_reassignment = "allow_host_reassignment_example"; // string | If true, the server may be moved to another host/port if the current host is unreachable

try {
    $apiInstance->postGameServerStart($server_id, $allow_host_reassignment);
} catch (Exception $e) {
    echo 'Exception when calling DefaultApi->postGameServerStart: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **server_id** | **string**|  |
 **allow_host_reassignment** | **string**| If true, the server may be moved to another host/port if the current host is unreachable | [optional]

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/x-www-form-urlencoded, multipart/form-data
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **postGameServerStop**
> postGameServerStop($server_id)

Stop a server

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Swagger\Client\Api\DefaultApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$server_id = "server_id_example"; // string | 

try {
    $apiInstance->postGameServerStop($server_id);
} catch (Exception $e) {
    echo 'Exception when calling DefaultApi->postGameServerStop: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **server_id** | **string**|  |

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **postGameServerSyncFiles**
> postGameServerSyncFiles($server_id)

Sync the files between the API cache and the gameserver

This manually triggers a sync between the gameserver and the local cache which is used for duplicating servers. This is done automatically approximately once per five minutes.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Swagger\Client\Api\DefaultApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$server_id = "server_id_example"; // string | 

try {
    $apiInstance->postGameServerSyncFiles($server_id);
} catch (Exception $e) {
    echo 'Exception when calling DefaultApi->postGameServerSyncFiles: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **server_id** | **string**|  |

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **postGameServerUnzip**
> postGameServerUnzip($server_id, $path, $destination)

Unzip a file on the GameServer

The file at path will be unzipped to destination

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Swagger\Client\Api\DefaultApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$server_id = "server_id_example"; // string | 
$path = "path_example"; // string | 
$destination = "destination_example"; // string | Unzip destination path

try {
    $apiInstance->postGameServerUnzip($server_id, $path, $destination);
} catch (Exception $e) {
    echo 'Exception when calling DefaultApi->postGameServerUnzip: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **server_id** | **string**|  |
 **path** | **string**|  |
 **destination** | **string**| Unzip destination path | [optional]

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/x-www-form-urlencoded, multipart/form-data
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **postGameServers**
> postGameServers($game, $name, $autostop, $autostop_minutes, $custom_domain, $enable_mysql, $location, $scheduled_commands, $user_data, $csgo_settings_autoload_configs, $csgo_settings_disable_bots, $csgo_settings_enable_csay_plugin, $csgo_settings_enable_gotv, $csgo_settings_enable_sourcemod, $csgo_settings_game_mode, $csgo_settings_insecure, $csgo_settings_mapgroup, $csgo_settings_mapgroup_start_map, $csgo_settings_maps_source, $csgo_settings_password, $csgo_settings_pure_server, $csgo_settings_rcon, $csgo_settings_slots, $csgo_settings_sourcemod_admins, $csgo_settings_sourcemod_plugins, $csgo_settings_steam_game_server_login_token, $csgo_settings_tickrate, $csgo_settings_workshop_authkey, $csgo_settings_workshop_id, $csgo_settings_workshop_start_map_id, $mumble_settings_password, $mumble_settings_slots, $mumble_settings_superuser_password, $mumble_settings_welcome_text, $teamfortress2_settings_autoload_configs, $teamfortress2_settings_enable_gotv, $teamfortress2_settings_enable_sourcemod, $teamfortress2_settings_insecure, $teamfortress2_settings_password, $teamfortress2_settings_rcon, $teamfortress2_settings_slots, $teamfortress2_settings_sourcemod_admins, $teamspeak3_settings_slots)

Create server

On success this returns the new server in the same format as game-servers.GET <br /><br /><h3>Currently available locations per game is:</h3><b>csgo</b>: amsterdam, barcelona, bristol, chicago, dallas, dusseldorf, istanbul, los_angeles, new_york_city, stockholm, strasbourg, sydney, warsaw<br /><b>mumble</b>: bristol, chicago, dallas, dusseldorf, los_angeles, stockholm, strasbourg, sydney, warsaw<br /><b>teamfortress2</b>: amsterdam, barcelona, bristol, chicago, dallas, dusseldorf, los_angeles, new_york_city, stockholm, strasbourg, sydney, warsaw<br /><b>teamspeak3</b>: amsterdam, barcelona, bristol, chicago, dallas, dusseldorf, los_angeles, new_york_city, stockholm, strasbourg, sydney, warsaw<br />

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Swagger\Client\Api\DefaultApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$game = "game_example"; // string | 
$name = "name_example"; // string | Name of the server
$autostop = "autostop_example"; // string | Automatically stop server when empty <autostop_minutes> min
$autostop_minutes = 56; // int | Minutes until autostop triggers if is is enabled
$custom_domain = "custom_domain_example"; // string | Use a custom domain for the server
$enable_mysql = "enable_mysql_example"; // string | Enable MySQL addon
$location = "location_example"; // string | Location of the server, see above for available locations.
$scheduled_commands = "scheduled_commands_example"; // string | JSON list with scheduled commands (see game-servers.GET for format to use)
$user_data = "user_data_example"; // string | Custom metadata for the server, is not used by the DatHost system
$csgo_settings_autoload_configs = "csgo_settings_autoload_configs_example"; // string | JSON list of configs to load automatically
$csgo_settings_disable_bots = "csgo_settings_disable_bots_example"; // string | Disable bots
$csgo_settings_enable_csay_plugin = "csgo_settings_enable_csay_plugin_example"; // string | Enable cSay plugin (required for eBot)
$csgo_settings_enable_gotv = "csgo_settings_enable_gotv_example"; // string | Enable GOTV
$csgo_settings_enable_sourcemod = "csgo_settings_enable_sourcemod_example"; // string | Enable Sourcemod
$csgo_settings_game_mode = "csgo_settings_game_mode_example"; // string | Game mode
$csgo_settings_insecure = "csgo_settings_insecure_example"; // string | Insecure server
$csgo_settings_mapgroup = "csgo_settings_mapgroup_example"; // string | Mapgroup
$csgo_settings_mapgroup_start_map = "csgo_settings_mapgroup_start_map_example"; // string | Mapgroup start map
$csgo_settings_maps_source = "csgo_settings_maps_source_example"; // string | Maps source
$csgo_settings_password = "csgo_settings_password_example"; // string | Server password
$csgo_settings_pure_server = "csgo_settings_pure_server_example"; // string | Pure server
$csgo_settings_rcon = "csgo_settings_rcon_example"; // string | RCON password
$csgo_settings_slots = 56; // int | Server slots
$csgo_settings_sourcemod_admins = "csgo_settings_sourcemod_admins_example"; // string | Sourcemod admins
$csgo_settings_sourcemod_plugins = "csgo_settings_sourcemod_plugins_example"; // string | JSON list of sourcemod plugin IDs
$csgo_settings_steam_game_server_login_token = "csgo_settings_steam_game_server_login_token_example"; // string | Steam Game Server Login Token
$csgo_settings_tickrate = 8.14; // float | Server tickrate
$csgo_settings_workshop_authkey = "csgo_settings_workshop_authkey_example"; // string | Workshop collection authkey, leave blank to use our default Authkey
$csgo_settings_workshop_id = "csgo_settings_workshop_id_example"; // string | Steam workshop ID
$csgo_settings_workshop_start_map_id = "csgo_settings_workshop_start_map_id_example"; // string | Steam ID of workshop start map
$mumble_settings_password = "mumble_settings_password_example"; // string | Server password
$mumble_settings_slots = 56; // int | Server slots
$mumble_settings_superuser_password = "mumble_settings_superuser_password_example"; // string | Superuser password
$mumble_settings_welcome_text = "mumble_settings_welcome_text_example"; // string | Welcome text
$teamfortress2_settings_autoload_configs = "teamfortress2_settings_autoload_configs_example"; // string | JSON list of configs to load automatically
$teamfortress2_settings_enable_gotv = "teamfortress2_settings_enable_gotv_example"; // string | Enable GOTV
$teamfortress2_settings_enable_sourcemod = "teamfortress2_settings_enable_sourcemod_example"; // string | Enable Sourcemod
$teamfortress2_settings_insecure = "teamfortress2_settings_insecure_example"; // string | Insecure server
$teamfortress2_settings_password = "teamfortress2_settings_password_example"; // string | Server password
$teamfortress2_settings_rcon = "teamfortress2_settings_rcon_example"; // string | Server RCON
$teamfortress2_settings_slots = 56; // int | Server slots
$teamfortress2_settings_sourcemod_admins = "teamfortress2_settings_sourcemod_admins_example"; // string | Sourcemod admins
$teamspeak3_settings_slots = 56; // int | Server slots

try {
    $apiInstance->postGameServers($game, $name, $autostop, $autostop_minutes, $custom_domain, $enable_mysql, $location, $scheduled_commands, $user_data, $csgo_settings_autoload_configs, $csgo_settings_disable_bots, $csgo_settings_enable_csay_plugin, $csgo_settings_enable_gotv, $csgo_settings_enable_sourcemod, $csgo_settings_game_mode, $csgo_settings_insecure, $csgo_settings_mapgroup, $csgo_settings_mapgroup_start_map, $csgo_settings_maps_source, $csgo_settings_password, $csgo_settings_pure_server, $csgo_settings_rcon, $csgo_settings_slots, $csgo_settings_sourcemod_admins, $csgo_settings_sourcemod_plugins, $csgo_settings_steam_game_server_login_token, $csgo_settings_tickrate, $csgo_settings_workshop_authkey, $csgo_settings_workshop_id, $csgo_settings_workshop_start_map_id, $mumble_settings_password, $mumble_settings_slots, $mumble_settings_superuser_password, $mumble_settings_welcome_text, $teamfortress2_settings_autoload_configs, $teamfortress2_settings_enable_gotv, $teamfortress2_settings_enable_sourcemod, $teamfortress2_settings_insecure, $teamfortress2_settings_password, $teamfortress2_settings_rcon, $teamfortress2_settings_slots, $teamfortress2_settings_sourcemod_admins, $teamspeak3_settings_slots);
} catch (Exception $e) {
    echo 'Exception when calling DefaultApi->postGameServers: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **game** | **string**|  |
 **name** | **string**| Name of the server |
 **autostop** | **string**| Automatically stop server when empty &lt;autostop_minutes&gt; min | [optional]
 **autostop_minutes** | **int**| Minutes until autostop triggers if is is enabled | [optional]
 **custom_domain** | **string**| Use a custom domain for the server | [optional]
 **enable_mysql** | **string**| Enable MySQL addon | [optional]
 **location** | **string**| Location of the server, see above for available locations. | [optional]
 **scheduled_commands** | **string**| JSON list with scheduled commands (see game-servers.GET for format to use) | [optional]
 **user_data** | **string**| Custom metadata for the server, is not used by the DatHost system | [optional]
 **csgo_settings_autoload_configs** | **string**| JSON list of configs to load automatically | [optional]
 **csgo_settings_disable_bots** | **string**| Disable bots | [optional]
 **csgo_settings_enable_csay_plugin** | **string**| Enable cSay plugin (required for eBot) | [optional]
 **csgo_settings_enable_gotv** | **string**| Enable GOTV | [optional]
 **csgo_settings_enable_sourcemod** | **string**| Enable Sourcemod | [optional]
 **csgo_settings_game_mode** | **string**| Game mode | [optional]
 **csgo_settings_insecure** | **string**| Insecure server | [optional]
 **csgo_settings_mapgroup** | **string**| Mapgroup | [optional]
 **csgo_settings_mapgroup_start_map** | **string**| Mapgroup start map | [optional]
 **csgo_settings_maps_source** | **string**| Maps source | [optional]
 **csgo_settings_password** | **string**| Server password | [optional]
 **csgo_settings_pure_server** | **string**| Pure server | [optional]
 **csgo_settings_rcon** | **string**| RCON password | [optional]
 **csgo_settings_slots** | **int**| Server slots | [optional]
 **csgo_settings_sourcemod_admins** | **string**| Sourcemod admins | [optional]
 **csgo_settings_sourcemod_plugins** | **string**| JSON list of sourcemod plugin IDs | [optional]
 **csgo_settings_steam_game_server_login_token** | **string**| Steam Game Server Login Token | [optional]
 **csgo_settings_tickrate** | **float**| Server tickrate | [optional]
 **csgo_settings_workshop_authkey** | **string**| Workshop collection authkey, leave blank to use our default Authkey | [optional]
 **csgo_settings_workshop_id** | **string**| Steam workshop ID | [optional]
 **csgo_settings_workshop_start_map_id** | **string**| Steam ID of workshop start map | [optional]
 **mumble_settings_password** | **string**| Server password | [optional]
 **mumble_settings_slots** | **int**| Server slots | [optional]
 **mumble_settings_superuser_password** | **string**| Superuser password | [optional]
 **mumble_settings_welcome_text** | **string**| Welcome text | [optional]
 **teamfortress2_settings_autoload_configs** | **string**| JSON list of configs to load automatically | [optional]
 **teamfortress2_settings_enable_gotv** | **string**| Enable GOTV | [optional]
 **teamfortress2_settings_enable_sourcemod** | **string**| Enable Sourcemod | [optional]
 **teamfortress2_settings_insecure** | **string**| Insecure server | [optional]
 **teamfortress2_settings_password** | **string**| Server password | [optional]
 **teamfortress2_settings_rcon** | **string**| Server RCON | [optional]
 **teamfortress2_settings_slots** | **int**| Server slots | [optional]
 **teamfortress2_settings_sourcemod_admins** | **string**| Sourcemod admins | [optional]
 **teamspeak3_settings_slots** | **int**| Server slots | [optional]

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/x-www-form-urlencoded, multipart/form-data
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **putGameServerFilesItem**
> putGameServerFilesItem($server_id, $path, $destination)

Move a file/directory on the gameserver

Moves path to destination, the paths are counted from the root node as seen in the file manager in the control panel, i.e. to delete csgo/cfg/server.cfg the path would be cfg/server.cfg.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Swagger\Client\Api\DefaultApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$server_id = "server_id_example"; // string | 
$path = "path_example"; // string | 
$destination = "destination_example"; // string | Destination path

try {
    $apiInstance->putGameServerFilesItem($server_id, $path, $destination);
} catch (Exception $e) {
    echo 'Exception when calling DefaultApi->putGameServerFilesItem: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **server_id** | **string**|  |
 **path** | **string**|  |
 **destination** | **string**| Destination path |

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/x-www-form-urlencoded, multipart/form-data
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **putGameServerItem**
> putGameServerItem($server_id, $autostop, $autostop_minutes, $custom_domain, $enable_mysql, $location, $name, $scheduled_commands, $user_data, $csgo_settings_autoload_configs, $csgo_settings_disable_bots, $csgo_settings_enable_csay_plugin, $csgo_settings_enable_gotv, $csgo_settings_enable_sourcemod, $csgo_settings_game_mode, $csgo_settings_insecure, $csgo_settings_mapgroup, $csgo_settings_mapgroup_start_map, $csgo_settings_maps_source, $csgo_settings_password, $csgo_settings_pure_server, $csgo_settings_rcon, $csgo_settings_slots, $csgo_settings_sourcemod_admins, $csgo_settings_sourcemod_plugins, $csgo_settings_steam_game_server_login_token, $csgo_settings_tickrate, $csgo_settings_workshop_authkey, $csgo_settings_workshop_id, $csgo_settings_workshop_start_map_id, $mumble_settings_password, $mumble_settings_slots, $mumble_settings_superuser_password, $mumble_settings_welcome_text, $teamfortress2_settings_autoload_configs, $teamfortress2_settings_enable_gotv, $teamfortress2_settings_enable_sourcemod, $teamfortress2_settings_insecure, $teamfortress2_settings_password, $teamfortress2_settings_rcon, $teamfortress2_settings_slots, $teamfortress2_settings_sourcemod_admins, $teamspeak3_settings_slots)

Update server details

If the server is on this will restart the server to reflect the changes. <h3>Currently available locations per game is:</h3><b>csgo</b>: amsterdam, barcelona, bristol, chicago, dallas, dusseldorf, istanbul, los_angeles, new_york_city, stockholm, strasbourg, sydney, warsaw<br /><b>mumble</b>: bristol, chicago, dallas, dusseldorf, los_angeles, stockholm, strasbourg, sydney, warsaw<br /><b>teamfortress2</b>: amsterdam, barcelona, bristol, chicago, dallas, dusseldorf, los_angeles, new_york_city, stockholm, strasbourg, sydney, warsaw<br /><b>teamspeak3</b>: amsterdam, barcelona, bristol, chicago, dallas, dusseldorf, los_angeles, new_york_city, stockholm, strasbourg, sydney, warsaw<br />

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Swagger\Client\Api\DefaultApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$server_id = "server_id_example"; // string | 
$autostop = "autostop_example"; // string | Automatically stop server when empty <autostop_minutes> min
$autostop_minutes = 56; // int | Minutes until autostop triggers if is is enabled
$custom_domain = "custom_domain_example"; // string | Use a custom domain for the server
$enable_mysql = "enable_mysql_example"; // string | Enable MySQL addon
$location = "location_example"; // string | Location of the server, see above for available locations.
$name = "name_example"; // string | Name of the server
$scheduled_commands = "scheduled_commands_example"; // string | JSON list with scheduled commands (see game-servers.GET for format to use)
$user_data = "user_data_example"; // string | Custom metadata for the server, is not used by the DatHost system
$csgo_settings_autoload_configs = "csgo_settings_autoload_configs_example"; // string | JSON list of configs to load automatically
$csgo_settings_disable_bots = "csgo_settings_disable_bots_example"; // string | Disable bots
$csgo_settings_enable_csay_plugin = "csgo_settings_enable_csay_plugin_example"; // string | Enable cSay plugin (required for eBot)
$csgo_settings_enable_gotv = "csgo_settings_enable_gotv_example"; // string | Enable GOTV
$csgo_settings_enable_sourcemod = "csgo_settings_enable_sourcemod_example"; // string | Enable Sourcemod
$csgo_settings_game_mode = "csgo_settings_game_mode_example"; // string | Game mode
$csgo_settings_insecure = "csgo_settings_insecure_example"; // string | Insecure server
$csgo_settings_mapgroup = "csgo_settings_mapgroup_example"; // string | Mapgroup
$csgo_settings_mapgroup_start_map = "csgo_settings_mapgroup_start_map_example"; // string | Mapgroup start map
$csgo_settings_maps_source = "csgo_settings_maps_source_example"; // string | Maps source
$csgo_settings_password = "csgo_settings_password_example"; // string | Server password
$csgo_settings_pure_server = "csgo_settings_pure_server_example"; // string | Pure server
$csgo_settings_rcon = "csgo_settings_rcon_example"; // string | RCON password
$csgo_settings_slots = 56; // int | Server slots
$csgo_settings_sourcemod_admins = "csgo_settings_sourcemod_admins_example"; // string | Sourcemod admins
$csgo_settings_sourcemod_plugins = "csgo_settings_sourcemod_plugins_example"; // string | JSON list of sourcemod plugin IDs
$csgo_settings_steam_game_server_login_token = "csgo_settings_steam_game_server_login_token_example"; // string | Steam Game Server Login Token
$csgo_settings_tickrate = 8.14; // float | Server tickrate
$csgo_settings_workshop_authkey = "csgo_settings_workshop_authkey_example"; // string | Workshop collection authkey, leave blank to use our default Authkey
$csgo_settings_workshop_id = "csgo_settings_workshop_id_example"; // string | Steam workshop ID
$csgo_settings_workshop_start_map_id = "csgo_settings_workshop_start_map_id_example"; // string | Steam ID of workshop start map
$mumble_settings_password = "mumble_settings_password_example"; // string | Server password
$mumble_settings_slots = 56; // int | Server slots
$mumble_settings_superuser_password = "mumble_settings_superuser_password_example"; // string | Superuser password
$mumble_settings_welcome_text = "mumble_settings_welcome_text_example"; // string | Welcome text
$teamfortress2_settings_autoload_configs = "teamfortress2_settings_autoload_configs_example"; // string | JSON list of configs to load automatically
$teamfortress2_settings_enable_gotv = "teamfortress2_settings_enable_gotv_example"; // string | Enable GOTV
$teamfortress2_settings_enable_sourcemod = "teamfortress2_settings_enable_sourcemod_example"; // string | Enable Sourcemod
$teamfortress2_settings_insecure = "teamfortress2_settings_insecure_example"; // string | Insecure server
$teamfortress2_settings_password = "teamfortress2_settings_password_example"; // string | Server password
$teamfortress2_settings_rcon = "teamfortress2_settings_rcon_example"; // string | Server RCON
$teamfortress2_settings_slots = 56; // int | Server slots
$teamfortress2_settings_sourcemod_admins = "teamfortress2_settings_sourcemod_admins_example"; // string | Sourcemod admins
$teamspeak3_settings_slots = 56; // int | Server slots

try {
    $apiInstance->putGameServerItem($server_id, $autostop, $autostop_minutes, $custom_domain, $enable_mysql, $location, $name, $scheduled_commands, $user_data, $csgo_settings_autoload_configs, $csgo_settings_disable_bots, $csgo_settings_enable_csay_plugin, $csgo_settings_enable_gotv, $csgo_settings_enable_sourcemod, $csgo_settings_game_mode, $csgo_settings_insecure, $csgo_settings_mapgroup, $csgo_settings_mapgroup_start_map, $csgo_settings_maps_source, $csgo_settings_password, $csgo_settings_pure_server, $csgo_settings_rcon, $csgo_settings_slots, $csgo_settings_sourcemod_admins, $csgo_settings_sourcemod_plugins, $csgo_settings_steam_game_server_login_token, $csgo_settings_tickrate, $csgo_settings_workshop_authkey, $csgo_settings_workshop_id, $csgo_settings_workshop_start_map_id, $mumble_settings_password, $mumble_settings_slots, $mumble_settings_superuser_password, $mumble_settings_welcome_text, $teamfortress2_settings_autoload_configs, $teamfortress2_settings_enable_gotv, $teamfortress2_settings_enable_sourcemod, $teamfortress2_settings_insecure, $teamfortress2_settings_password, $teamfortress2_settings_rcon, $teamfortress2_settings_slots, $teamfortress2_settings_sourcemod_admins, $teamspeak3_settings_slots);
} catch (Exception $e) {
    echo 'Exception when calling DefaultApi->putGameServerItem: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **server_id** | **string**|  |
 **autostop** | **string**| Automatically stop server when empty &lt;autostop_minutes&gt; min | [optional]
 **autostop_minutes** | **int**| Minutes until autostop triggers if is is enabled | [optional]
 **custom_domain** | **string**| Use a custom domain for the server | [optional]
 **enable_mysql** | **string**| Enable MySQL addon | [optional]
 **location** | **string**| Location of the server, see above for available locations. | [optional]
 **name** | **string**| Name of the server | [optional]
 **scheduled_commands** | **string**| JSON list with scheduled commands (see game-servers.GET for format to use) | [optional]
 **user_data** | **string**| Custom metadata for the server, is not used by the DatHost system | [optional]
 **csgo_settings_autoload_configs** | **string**| JSON list of configs to load automatically | [optional]
 **csgo_settings_disable_bots** | **string**| Disable bots | [optional]
 **csgo_settings_enable_csay_plugin** | **string**| Enable cSay plugin (required for eBot) | [optional]
 **csgo_settings_enable_gotv** | **string**| Enable GOTV | [optional]
 **csgo_settings_enable_sourcemod** | **string**| Enable Sourcemod | [optional]
 **csgo_settings_game_mode** | **string**| Game mode | [optional]
 **csgo_settings_insecure** | **string**| Insecure server | [optional]
 **csgo_settings_mapgroup** | **string**| Mapgroup | [optional]
 **csgo_settings_mapgroup_start_map** | **string**| Mapgroup start map | [optional]
 **csgo_settings_maps_source** | **string**| Maps source | [optional]
 **csgo_settings_password** | **string**| Server password | [optional]
 **csgo_settings_pure_server** | **string**| Pure server | [optional]
 **csgo_settings_rcon** | **string**| RCON password | [optional]
 **csgo_settings_slots** | **int**| Server slots | [optional]
 **csgo_settings_sourcemod_admins** | **string**| Sourcemod admins | [optional]
 **csgo_settings_sourcemod_plugins** | **string**| JSON list of sourcemod plugin IDs | [optional]
 **csgo_settings_steam_game_server_login_token** | **string**| Steam Game Server Login Token | [optional]
 **csgo_settings_tickrate** | **float**| Server tickrate | [optional]
 **csgo_settings_workshop_authkey** | **string**| Workshop collection authkey, leave blank to use our default Authkey | [optional]
 **csgo_settings_workshop_id** | **string**| Steam workshop ID | [optional]
 **csgo_settings_workshop_start_map_id** | **string**| Steam ID of workshop start map | [optional]
 **mumble_settings_password** | **string**| Server password | [optional]
 **mumble_settings_slots** | **int**| Server slots | [optional]
 **mumble_settings_superuser_password** | **string**| Superuser password | [optional]
 **mumble_settings_welcome_text** | **string**| Welcome text | [optional]
 **teamfortress2_settings_autoload_configs** | **string**| JSON list of configs to load automatically | [optional]
 **teamfortress2_settings_enable_gotv** | **string**| Enable GOTV | [optional]
 **teamfortress2_settings_enable_sourcemod** | **string**| Enable Sourcemod | [optional]
 **teamfortress2_settings_insecure** | **string**| Insecure server | [optional]
 **teamfortress2_settings_password** | **string**| Server password | [optional]
 **teamfortress2_settings_rcon** | **string**| Server RCON | [optional]
 **teamfortress2_settings_slots** | **int**| Server slots | [optional]
 **teamfortress2_settings_sourcemod_admins** | **string**| Sourcemod admins | [optional]
 **teamspeak3_settings_slots** | **int**| Server slots | [optional]

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/x-www-form-urlencoded, multipart/form-data
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

