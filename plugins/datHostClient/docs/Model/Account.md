# Account

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**credits** | **float** | Number of credits left | 
**seconds_left** | **int** | Approximately seconds the credits will last with current usage. Returns null if no servers are on | [optional] 
**time_left** | **string** | Human readable approximate of the time the credits will last with current usage, for seconds see seconds_left | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


