# CsgoSettings

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**slots** | **int** | Server slots | [optional] 
**steam_game_server_login_token** | **string** | Steam Game Server Login Token | [optional] 
**rcon** | **string** | RCON password | [optional] 
**password** | **string** | Server password | [optional] 
**maps_source** | **string** | Maps source (mapgroup or workshop) | [optional] 
**mapgroup** | **string** | Mapgroup | [optional] 
**mapgroup_start_map** | **string** | Mapgroup start map | [optional] 
**workshop_id** | **string** | Steam workshop ID | [optional] 
**workshop_start_map_id** | **string** | Steam ID of workshop start map | [optional] 
**workshop_authkey** | **string** | Workshop collection authkey, leave blank to use our default Authkey | [optional] 
**autoload_configs** | **string[]** | List of configs to load automatically | [optional] 
**sourcemod_admins** | **string** | Sourcemod admins | [optional] 
**sourcemod_plugins** | **string[]** | Sourcemod plugins | [optional] 
**enable_gotv** | **bool** | Enable GOTV | [optional] 
**enable_sourcemod** | **bool** | Enable Sourcemod | [optional] 
**enable_csay_plugin** | **bool** | Enable cSay plugin (required for eBot) | [optional] 
**game_mode** | **string** | Game mode | [optional] 
**tickrate** | **float** | Server tickrate | [optional] 
**pure_server** | **bool** | Pure server | [optional] 
**insecure** | **bool** | Insecure server | [optional] 
**disable_bots** | **bool** | Disable bots | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


