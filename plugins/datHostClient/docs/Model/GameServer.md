# GameServer

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **string** | Server ID | 
**name** | **string** | Name of the server | 
**user_data** | **string** | Custom metadata for the server,is not used by the DatHost system | 
**game** | **string** | A lowercase string identifying the game that this server is running | 
**location** | **string** | Server location | 
**players_online** | **int** | Number of players online | 
**status** | [**\Swagger\Client\Model\KeyValue[]**](KeyValue.md) | Various server status (map, version etc.) | [optional] 
**booting** | **bool** | Is the server in booting state? | 
**server_error** | **string** | Have we detected an error on the server? | [optional] 
**ip** | **string** | Hostname or IP to gameserver | 
**on** | **bool** | Is the server currently on? | 
**ports** | [**\Swagger\Client\Model\Ports**](Ports.md) |  | [optional] 
**confirmed** | **bool** | Has the server been ordered yet (used internally) | 
**cost_per_hour** | **float** | Current cost per hour for this server | 
**month_credits** | **float** | Credits this server has used this month | 
**month_reset_at** | **int** | UNIX timestamp when the month_credits will be reset | 
**max_cost_per_month** | **float** | Max cost per month for this server | 
**enable_mysql** | **bool** | Enable MySQL addon | 
**autostop** | **bool** | Automatically stop server when empty &lt;autostop_minutes&gt; min | 
**autostop_minutes** | **int** | Minutes until autostop triggers if is is enabled | 
**mysql_username** | **string** | MySQL username | 
**mysql_password** | **string** | MySQL password | 
**ftp_password** | **string** | FTP password | 
**disk_usage_bytes** | **int** | Disk usage in bytes, only available in single GET | [optional] 
**default_file_locations** | **string[]** | Physical location of the server addons (used internally) | [optional] 
**custom_domain** | **string** | Use a custom domain for the server | [optional] 
**scheduled_commands** | [**\Swagger\Client\Model\ScheduledCommand[]**](ScheduledCommand.md) |  | [optional] 
**added_voice_server** | **string** | ID of a voice server that was purchased with this server | [optional] 
**csgo_settings** | **object** | (CS:GO only) Settings specific to CS:GO | [optional] 
**mumble_settings** | **object** | (Mumble only) Settings specific to Mumble | [optional] 
**teamfortress2_settings** | **object** | (Team Fortress 2 only) Settings specific to teamfortress2 | [optional] 
**teamspeak3_settings** | **object** | (Teamspeak 3 only) Settings specific to Teamspeak 3 | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


