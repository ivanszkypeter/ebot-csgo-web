# MapsPlayed

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**map** | **string** | Map name | [optional] 
**seconds** | **int** | Approx. seconds played | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


