# Metrics

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**all_time_players** | **object** | All time players stats | [optional] 
**players_online** | **object** | Current players online | [optional] 
**players_online_graph** | **object** | Players online graph | [optional] 
**maps_played** | **object** | Maps played | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


