# MumbleSettings

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**slots** | **int** | Server slots | [optional] 
**password** | **string** | Server password | [optional] 
**superuser_password** | **string** | Superuser password | [optional] 
**welcome_text** | **string** | Welcome text | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


