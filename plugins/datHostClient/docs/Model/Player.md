# Player

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**name** | **string** | Player nickname | [optional] 
**duration** | **int** | How long the player has been online (only CS:GO and TeamFortress 2) | [optional] 
**score** | **int** | Player score (only CS:GO and TeamFortress 2) | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


