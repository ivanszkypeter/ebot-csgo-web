# Ports

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**game** | **int** | Main game/voice port | 
**gotv** | **int** | (CS:GO only) Port used for gotv streaming | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


