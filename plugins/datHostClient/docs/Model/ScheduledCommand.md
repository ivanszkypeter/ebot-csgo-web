# ScheduledCommand

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**name** | **string** |  | [optional] 
**action** | **string** |  | [optional] 
**command** | **string** |  | [optional] 
**run_at** | **int** |  | [optional] 
**repeat** | **int** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


