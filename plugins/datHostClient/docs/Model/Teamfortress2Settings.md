# Teamfortress2Settings

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**slots** | **int** | Server slots | [optional] 
**rcon** | **string** | Server RCON | [optional] 
**password** | **string** | Server Password | [optional] 
**autoload_configs** | **string[]** | List of configs to load automatically | [optional] 
**sourcemod_admins** | **string** | Sourcemod admins | [optional] 
**enable_gotv** | **bool** | Enable GOTV | [optional] 
**enable_sourcemod** | **bool** | Enable Sourcemod | [optional] 
**insecure** | **bool** | Insecure server | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


