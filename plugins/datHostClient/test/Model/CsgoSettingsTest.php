<?php
/**
 * CsgoSettingsTest
 *
 * PHP version 5
 *
 * @category Class
 * @package  Swagger\Client
 * @author   Swagger Codegen team
 * @link     https://github.com/swagger-api/swagger-codegen
 */

/**
 * API
 *
 * No description provided (generated by Swagger Codegen https://github.com/swagger-api/swagger-codegen)
 *
 * OpenAPI spec version: 1.0
 * 
 * Generated by: https://github.com/swagger-api/swagger-codegen.git
 * Swagger Codegen version: 2.4.14-SNAPSHOT
 */

/**
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen
 * Please update the test case below to test the model.
 */

namespace Swagger\Client;

/**
 * CsgoSettingsTest Class Doc Comment
 *
 * @category    Class
 * @description CsgoSettings
 * @package     Swagger\Client
 * @author      Swagger Codegen team
 * @link        https://github.com/swagger-api/swagger-codegen
 */
class CsgoSettingsTest extends \PHPUnit_Framework_TestCase
{

    /**
     * Setup before running any test case
     */
    public static function setUpBeforeClass()
    {
    }

    /**
     * Setup before running each test case
     */
    public function setUp()
    {
    }

    /**
     * Clean up after running each test case
     */
    public function tearDown()
    {
    }

    /**
     * Clean up after running all test cases
     */
    public static function tearDownAfterClass()
    {
    }

    /**
     * Test "CsgoSettings"
     */
    public function testCsgoSettings()
    {
    }

    /**
     * Test attribute "slots"
     */
    public function testPropertySlots()
    {
    }

    /**
     * Test attribute "steam_game_server_login_token"
     */
    public function testPropertySteamGameServerLoginToken()
    {
    }

    /**
     * Test attribute "rcon"
     */
    public function testPropertyRcon()
    {
    }

    /**
     * Test attribute "password"
     */
    public function testPropertyPassword()
    {
    }

    /**
     * Test attribute "maps_source"
     */
    public function testPropertyMapsSource()
    {
    }

    /**
     * Test attribute "mapgroup"
     */
    public function testPropertyMapgroup()
    {
    }

    /**
     * Test attribute "mapgroup_start_map"
     */
    public function testPropertyMapgroupStartMap()
    {
    }

    /**
     * Test attribute "workshop_id"
     */
    public function testPropertyWorkshopId()
    {
    }

    /**
     * Test attribute "workshop_start_map_id"
     */
    public function testPropertyWorkshopStartMapId()
    {
    }

    /**
     * Test attribute "workshop_authkey"
     */
    public function testPropertyWorkshopAuthkey()
    {
    }

    /**
     * Test attribute "autoload_configs"
     */
    public function testPropertyAutoloadConfigs()
    {
    }

    /**
     * Test attribute "sourcemod_admins"
     */
    public function testPropertySourcemodAdmins()
    {
    }

    /**
     * Test attribute "sourcemod_plugins"
     */
    public function testPropertySourcemodPlugins()
    {
    }

    /**
     * Test attribute "enable_gotv"
     */
    public function testPropertyEnableGotv()
    {
    }

    /**
     * Test attribute "enable_sourcemod"
     */
    public function testPropertyEnableSourcemod()
    {
    }

    /**
     * Test attribute "enable_csay_plugin"
     */
    public function testPropertyEnableCsayPlugin()
    {
    }

    /**
     * Test attribute "game_mode"
     */
    public function testPropertyGameMode()
    {
    }

    /**
     * Test attribute "tickrate"
     */
    public function testPropertyTickrate()
    {
    }

    /**
     * Test attribute "pure_server"
     */
    public function testPropertyPureServer()
    {
    }

    /**
     * Test attribute "insecure"
     */
    public function testPropertyInsecure()
    {
    }

    /**
     * Test attribute "disable_bots"
     */
    public function testPropertyDisableBots()
    {
    }
}
